FROM centos:8
USER root
WORKDIR "/root/.install/bashellite"
ENV SYS_DEPS="bash which coreutils grep"
ENV PROGNAME="bashellite"
ENV USERNAME="${PROGNAME}"
ARG UID=1006
ENV HOMEDIR="/home/${USERNAME}"
ENV PATHDIR="/usr/local"
ENV BINDIR="${PATHDIR}/bin"
ENV LIBDIR="${PATHDIR}/lib"
ENV TERM="xterm-256color"
ADD . .
RUN { echo -e "\n[INFO] Installing system deps."; \
      { for SYS_DEP in ${SYS_DEPS}; do \
          echo -e "\n[INFO] Installing sys dep: ${SYS_DEP}"; \
          yum install -y --allowerasing "${SYS_DEP}"; \
        done; } || exit 1; } && \
    { echo -e "\n[INFO] Setting up user (${USERNAME})."; \
      { adduser -u ${UID} -d "${HOMEDIR}" -s "/bin/bash" "${USERNAME}"; } || true; } && \
    { echo -e "\n[INFO] Creating and setting up perms for ${HOMEDIR}."; \
      { mkdir -p "${HOMEDIR}" && \
        chown -R "${USERNAME}:${USERNAME}" "${HOMEDIR}" && \
        chmod 2770 "${HOMEDIR}"; } || exit 1; } && \
    { echo -e "\n[INFO] Installing required libraries into libraries directory."; \
      { mkdir -p "${LIBDIR}" && \
        chown -R "root:root" "${LIBDIR}" && \
        chmod 0775 "${LIBDIR}" && \
        install \
          -o "root" -g "bashellite" -m 0440 \
          -D "./usr/local/lib/bashellite-libs.sh" "${LIBDIR}/bashellite-libs.sh" && \
        install \
          -o "root" -g "root" -m 0444 \
          -D "./usr/local/lib/util-libs.sh" "${LIBDIR}/util-libs.sh"; } || exit 1; } && \
    { echo -e "\n[INFO] Installing bashellite cli into bin directory."; \
      { mkdir -p "${BINDIR}" && \
        chown -R "root:root" "${BINDIR}" && \
        chmod 0775 "${BINDIR}" && \
        install \
          -o "root" -g "bashellite" -m 0550 \
          -D "./usr/local/bin/bashellite.sh" "${BINDIR}/bashellite"; } || exit 1; };
USER ${USERNAME}
WORKDIR ${HOMEDIR}
RUN { echo -e "\n[INFO] Adding bashellite bin location to path via .bashrc." && \
      echo "export PATH=/usr/local/bin:$PATH" >> "${HOMEDIR}/.bashrc" || exit 1; }
