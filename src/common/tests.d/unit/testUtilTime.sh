#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
  # shellcheck source=usr/local/lib/bashellite-libs.sh
  source ${lib_dir}/bashellite-libs.sh;
}

testUtilTime_InvalidTime() {

  rslt=$( date() {
      if [[ "$*" == *"--version"* ]]; then
        echo 'A GNU date'
      else
        echo "2018-05-07"
      fi
    };
    utilTime 2>&1 )

  assertFalse $?;

}

testUtilTime_Success(){

  # Tests success condition
  rslt=$( date() {
    if [[ "$*" == *"--version"* ]]; then
      echo 'a GNU date thingy'
    else
      echo "2018-05-07T13:54:24,038845689-05:00"
    fi
  }; echo; utilTime)

  assertTrue $?
  assertContains "13542403" "$rslt";

}

# shellcheck source=/dev/null
source "$(which shunit2)";
