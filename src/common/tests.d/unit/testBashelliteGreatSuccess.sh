#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
  # shellcheck source=usr/local/lib/bashellite-libs.sh
  source ${lib_dir}/bashellite-libs.sh;
}

fixture_test_log() {

  local test_log='/var/log/bashellite/test.1.2.error.log'
  if [[ "$1" == 'cleanup' ]]; then
    rm -rf "$(dirname $test_log)";
  else
    mkdir -p "$(dirname $test_log)"
    echo 'errors' > $test_log;
  fi
}

testGreatSuccessErrorLog() {
  fixture_test_log

  rslt=$(
    _n_repo_name='test' _r_datestamp=1 _r_run_id=2 bashelliteGreatSuccess 2>&1;
  );

  assertTrue $?
  assertContains "${rslt}" "Please see error log for error details; minor errors detected in this run's error log:"

  fixture_test_log cleanup
}

testGreatSuccessNoErrorLog() {

  rslt=$(
    _n_repo_name='test' _r_datestamp=1 _r_run_id=2 bashelliteGreatSuccess 2>&1;
  );

  assertTrue $?
  assertContains "${rslt}" ""

}

# shellcheck source=/dev/null
source "$(which shunit2)";
