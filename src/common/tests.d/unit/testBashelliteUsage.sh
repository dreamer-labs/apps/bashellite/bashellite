#!/usr/bin/env bash

# shellcheck disable=SC1091

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

oneTimeSetUp() {
  local lib_dir
  lib_dir=$(realpath "${MY_DIR}/../../bases.d/.data/usr/local/lib")

  # shellcheck source=usr/local/lib/util-libs.sh
  source ${lib_dir}/util-libs.sh;
  # shellcheck source=usr/local/lib/bashellite-libs.sh
  source ${lib_dir}/bashellite-libs.sh;
}

testSingleParam() {

  for level in FAIL WARN INFO SKIP;
  do
    rslt=$(bashelliteUsage $level 2>&1);
    assertTrue $?
    assertContains "$rslt" "$level"
  done

}

testNoParam() {

  rslt=$(bashelliteUsage 2>&1);
  assertTrue $?
  assertContains "$rslt" "INFO"

}

testBadParam() {

  rslt=$(bashelliteUsage ABCDEFG 2>&1);
  assertFalse $?
  assertContains "$rslt" "Invalid input for function"

}

# shellcheck source=/dev/null
source "$(which shunit2)";
