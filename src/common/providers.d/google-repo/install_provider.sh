#!/usr/bin/env bash

main() {

  local provider_name="google-repo"
  local deps=( "git" "python3-virtualenv" "python3-pip" );
  local bins=( "git" "pip3" "virtualenv" );
  local bin_check_failed="false";

  for dep in "${deps[@]}"; do
    dnf -y install ${dep};
  done;
	  
  for bin in "${bins[@]}"; do
    which ${bin} &>/dev/null \
    || {
         echo "[WARN] Can not proceed until ${bin} is installed and accessible in path.";
         local bin_check_failed="true";
       };
  done;
  if [[ "${bin_check_failed}" == "true" ]]; then
    echo "[FAIL] ${provider_name} provider can not be installed until missing dependencies are installed; exiting.";
    exit 1;
  fi;
  
  # If git is installed, ensure google's repo is installed in proper location, and functional.
  # If google's repo is not installed, or broken, blow away the old one, and install a new one.
  if [ ! -s "/usr/local/bin/repo" ]
  then
    echo "[WARN] google's repo does NOT appear to be installed, (or it is broken); (re)installing..." \
    && curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo \
    && chmod 550 /usr/local/bin/repo \
    && chown root:bashellite /usr/local/bin/repo;

    # Ensure google repo installed successfully
    if [ -s "/usr/local/bin/repo" ]
    then
      echo "[INFO] ${provider_name} installed successfully..."
    else
      echo "[FAIL] ${provider_name} was NOT installed successfully; exiting." \
      && exit 1;
    fi
  else
    echo "[INFO] ${provider_name} provider successfully installed.";
  fi

}

main
