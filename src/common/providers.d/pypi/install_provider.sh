#!/usr/bin/env bash

main() {

  #local providers_tld="/opt/bashellite/providers.d";

  local provider_name="pypi"
  local deps=( "python3-pip" );
  local bins=( "pip3" "pypi-downloader" );
  local bin_check_failed="false";

  for dep in "${deps[@]}"; do
    dnf -y install ${dep};
  done;

  pip3 install pypi-downloader
	  
  for bin in "${bins[@]}"; do
    which ${bin} &>/dev/null \
    || {
         echo "[WARN] Can not proceed until ${bin} is installed and accessible in path.";
         local bin_check_failed="true";
       };
  done;
  if [[ "${bin_check_failed}" == "true" ]]; then
    echo "[FAIL] ${provider_name} provider can not be installed until missing dependencies are installed; exiting.";
    exit 1;
  fi;
  echo "[INFO] ${provider_name} provider successfully installed.";
  
  # for dep in \
  #            pip3 \
  #            virtualenv \
  #            rm \
  #            ; do
  #   which ${dep} &>/dev/null \
  #   || {
  #        echo "[FAIL] Can not proceed until ${dep} is installed and accessible in path; exiting." \
  #        && exit 1;
  #      };
  # done
  
  # If pip and virtualenv are installed, ensure pypi-downloader is installed in proper location, and functional.
  # If pypi-downloader is not installed, or broken, blow away the old one, and install a new one.
  # ${providers_tld}/pypi/exec/bin/pypi-downloader --help &>/dev/null \
  # || {
  #      echo "[WARN] pypi-downloader does NOT appear to be installed, (or it is broken); (re)installing..." \
  #      && rm -fr ${providers_tld}/pypi/exec/ &>/dev/null \
  #      && virtualenv --python=python3.6 ${providers_tld}/pypi/exec/ \
  #      && ${providers_tld}/pypi/exec/bin/pip3 install pypi-downloader
  #    };
  # Ensure pypi-downloader installed successfully
  # {
  #   ${providers_tld}/pypi/exec/bin/pypi-downloader --help &>/dev/null \
  #   && echo "[INFO] pypi-downloader installed successfully...";
  # } \
  # || {
  #      echo "[FAIL] pypi-downloader was NOT installed successfully; exiting." \
  #      && exit 1;
  #    };
}

main
