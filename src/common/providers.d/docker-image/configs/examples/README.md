#Example Configs

This directory contains one or more numbered subdirectories containing example configs for this provider designed to demonstrate how to properly format the provider's `repo.conf` and `provider.conf` files.

This directory also contains an example 'env.conf' file that contains example username and password example for use with logging into a remote registry if required.

