#!/usr/bin/env bash

main() {

  local provider_name="reposync"
  local deps=( "epel-release" "createrepo" "crudini" "yum-utils" );
  local bins=( "createrepo" "crudini" "reposync" );
  local bin_check_failed="false";

  for dep in "${deps[@]}"; do
    dnf -y install ${dep};
  done;
	  
  for bin in "${bins[@]}"; do
    which ${bin} &>/dev/null \
    || {
         echo "[WARN] Can not proceed until ${bin} is installed and accessible in path.";
         local bin_check_failed="true";
       };
  done;
  if [[ "${bin_check_failed}" == "true" ]]; then
    echo "[FAIL] ${provider_name} provider can not be installed until missing dependencies are installed; exiting.";
    exit 1;
  fi;
  echo "[INFO] ${provider_name} provider successfully installed.";

  # local bin_name="reposync";
  # local dep_check_failed="false";

  # for dep in \
  #            ${bin_name} \
  #            createrepo \
  #            crudini \
  #            ; do
  #   which ${dep} &>/dev/null \
  #   || {
  #        echo "[FAIL] Can not proceed until ${dep} is installed and accessible in path.";
  #        local dep_check_failed="true";
  #      };
  # done;
  # if [[ "${dep_check_failed}" == "true" ]]; then
  #   echo "[FAIL] ${bin_name} provider can not be installed until missing dependencies are installed; exiting." \
  #   && exit 1;
  # fi;
  # echo "[INFO] ${bin_name} provider successfully installed.";
}

main
