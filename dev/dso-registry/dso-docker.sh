# Set vars based on passed in vars
config_file="/home/chandler/projects/dso-registry/dso.conf"
skopeo_registry_url="registry1.dso.mil"
mirror_tld="/mnt/disk1/transfers"
mirror_repo_name="dso-images"
dryrun=""

mkdir -p ${mirror_tld}/${mirror_repo_name}

image_name_array=()

for line in $(cat ${config_file}); do
    echo "Processing line: ${line}"
    orig_line=${line}
    repo_line=${line%%:*}
    # Here we need to check the image name to see if it doesn't use a username.  If if doesn't, we will use the
    # default docker username 'library'
    IFS=$'/\n'
    repo_line_array=( ${repo_line} )
    unset IFS
    if [[ ${#repo_line_array[@]} == 1 ]]; then
        repo_line="library/${repo_line}"
    fi
    image_name=${repo_line//\//__}
    
    # Check to see if tags are listed
    tag_index=0
    tag_index=`expr index "${line}" ':'`
    tags_found=""
    tags_found="${line:${tag_index}}"

    if [[ ${tag_index} == 1 || ${tags_found} == "" ]]; then
        echo "Invalid image/tag format found: ${orig_line}, skipping..."
    else
        if [[ ${tag_index} == 0 ]]; then
            # Didn't find any tags, so setting tag to 'latest'
            echo "Tags not found, using 'latest' tag"
            tags_found="latest"
        else
            # Tags found
            echo "Tags found"
        fi

        # Create tags array to cycle through
        IFS=$',\n'
        tags_array=( ${tags_found} )
        unset IFS

        # Here we need to make a back up of the tag array, then reset the tag array
        # and go through the copy.  If we find a regular tag, we add it to the new tag array.
        # If we find a regex, we get a list of all the tags using the dockertags helper scripts and
        # compare them against the regex.  On a match, add those tags to the new tag array.

        orig_tags_array=${tags_array[@]}
        new_tags_array=()

        tag_list=""

        for each_tag in ${orig_tags_array[@]}; do
            if [[ ${each_tag:0:1} != "/" ]]; then
                new_tags_array+=" ${each_tag}"
            else
                # Possible regex tag, check for ending delimiter
                if [[ ${each_tag: -1} != "/" ]]; then
                    echo "Invalid regex tag format found: ${each_tag}, skipping..."
                else
                    echo "Here"
                    # Here we have a good regex, now we download all the tags for this image if they haven't already been downloaded
                    if [[ ${tag_list} == "" ]]; then
                        # tag_list=$(${exec_dir}/dockertagsv1.sh ${repo_username}/${image_name})
                        # Here we replace the use of dockertagsv1.sh with Auth token checking and use to access the 
                        # list of tags in a v2 compliant rapid way
                        # First, do a header check to see where to get auth token
                        token=""
                        auth_info=""
                        # Here we get the auth info by making a request with an invalid token to the docker endpoint
                        while read line; do
                            if [[ ${line} =~ Www-Authenticate ]]; then
                                # We need to get auth token using info from the Www-Authenticate header...
                                auth_info=${line//*Bearer }
                                IFS=$',\n'
                                auth_info=( ${auth_info} )
                                unset IFS
                                # Here we clean up the parameters received from the previous request
                                realm=${auth_info[0]#realm=}
                                realm=${realm//\"/}
                                param1=${auth_info[1]//\"}
                                param1=${param1//:/%3A}
                                param1=${param1//\//%2F}
                                param1=${param1//$'\r'}
                                param2=${auth_info[2]//\"}
                                param2=${param2//:/%3A}
                                param2=${param2//\//%2F}
                                param2=${param2//$'\r'}
                                url="${realm}?${param1}&${param2}"
                                # Here we get a valid token that is valid for 300 sec
                                token=$(curl -s "${url}" | jq -r '.token')
                                echo "Token used: $token"
                                break
                            fi
                        done < <(curl -I -s -H "Authorization: Bearer ${token}" "${_n_repo_url}/v2/${repo_line}/tags/list");

                        # Here we use the token to get the list of tags for the image being requested
                        tag_list=$(curl -s --request 'GET' -H "Authorization: Bearer ${token}" ${_n_repo_url}/v2/${repo_line}/tags/list | jq -r '.tags[]')

                    fi
                    # Next we grep the tag_list against the regex (each_tag)
                    for tag in ${tag_list}; do
                        tmpregextag=${each_tag#/}
                        regex_tag=${tmpregextag%/}
                        echo ${tag} | egrep -E "${regex_tag}" > /dev/null
                        if [[ ${?} == 0 ]]; then
                            new_tags_array+=" ${tag}"
                        fi
                    done
                fi
            fi
        done

        # Next we will cycle through each tag, check for an already saved file, and use skopeo to inspect the saved image and the online image and
        # compare image creation dates
        for each_tag in ${new_tags_array[@]}; do
            echo "Processing image: ${repo_line}:${each_tag}..."
            save_loc="${mirror_tld}/${mirror_repo_name}"
            image_file_name="${image_name}___${each_tag}.tar"
            
            # Check to see if a corresponding file exists for the image with this tag
            if [ -s ${save_loc}/${image_file_name} ]; then
                echo "Previously saved file found: ${save_loc}/${image_file_name}..."
                echo "Checking saved image creation timestamp..."
                local_timestamp=$(skopeo inspect docker-archive:${save_loc}/${image_file_name} | jq -r '.Created')
                echo "Checking online image creation timestamp..."
                online_timestamp=$(skopeo inspect docker://${skopeo_registry_url}/${repo_line}:${each_tag} | jq -r '.Created')
                
                # Next with we compare the local timestamp to the online timestamp and save if online timestamp is newer
                if [[ (${local_timestamp} == ${online_timestamp}) || (${local_timestamp} > ${online_timestamp}) ]]; then
                # Newest image already saved, skipping...
                echo "Saved image: ${repo_line}:${each_tag} is already current, skipping..."
                continue
                fi
            fi

            # Need to download and save the image
            repo_image="${skopeo_registry_url}/${repo_line}:${each_tag}"
            echo "Saving tag: ${each_tag} for image: ${repo_line}"
            echo "Command: skopeo copy docker://${repo_image} docker-archive:${save_loc}/${image_file_name}"
            if [[ ${dryrun} == "" ]]; then
                # Only save if not a dry run
                # # Check if file already exists, if it does, check to see if it is current
                if [ -s ${save_loc}/${image_file_name} ]; then
                    # Need to remove the old file before we can save the new one
                    rm -f ${save_loc}/${image_file_name}
                fi
                #skopeo copy --dest-creds patrick.chandler.ctr:2sb9fyqm4hrq7cvkq2dqgkn1m6d3owrn docker://${repo_image} docker-archive:${save_loc}/${image_file_name}
            fi
        done
    fi
done

# Need to clean out possible old left behind tmp files located in /var/tmp that may have been left behind by errored
# skopeo runs
rm -rf /var/tmp/docker-tarfile-blob*

unset skopeo_registry_url;
